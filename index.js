//khai bao thu vien express
const express = require("express");

//khai bao app 
const app = express();

//khai bao port
const port = 8000;

const methodMiddleware = (req, res, next) => {
    console.log(req.method);

    next();
};

app.use((req, res, next) => {
    console.log(new Date());

    next();
}, methodMiddleware);

/*
app.use((req, res, next) => {
    console.log(req.method);

    next();
});
*/

app.post('/', (req, res) => {
    console.log('Post method');
    res.status(200).json({
        message: 'Post method'
    })
})

app.get('/', (req, res) => {
    let today = new Date();
    console.log(`Hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`);
    res.status(200).json({
        message: `Hôm nay là ngày ${today.getDate()} tháng ${today.getMonth() + 1} năm ${today.getFullYear()}`
    })
})

//khoi dong app
app.listen(port, () => {
    console.log(`App listening on port ${port}`);
})